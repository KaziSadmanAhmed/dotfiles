function glgdm --description 'git pretty log with devmoji'
    command git log --graph --pretty=format:'%C(bold yellow)%h%Creset %C(brightblack)-%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --decorate --color --date=local | devmoji --log --color | less -RF
end
