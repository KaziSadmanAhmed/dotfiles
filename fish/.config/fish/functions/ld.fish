function ld --description 'list directories with exa' --wraps exa
    command exa --color always --icons --long --group --only-dirs $argv
end
