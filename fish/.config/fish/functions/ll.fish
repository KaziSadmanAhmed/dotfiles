function ll --description 'long list files in directory with exa' --wraps exa
	# Use --git argument if the directory is a part of a git repository
	if git rev-parse --is-inside-work-tree &>/dev/null
    	command exa --color always --icons --long --group --group-directories-first --git $argv
	else
    	command exa --color always --icons --long --group --group-directories-first $argv
	end
end
