function lr --description 'list files (sorted by most recent) with exa' --wraps exa
    command exa --color always --icons --long --group --group-directories-first --sort newest $argv
end
