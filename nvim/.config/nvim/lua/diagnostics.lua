local M = {}

M.update_signs = function()
    local signs = {
        Error = "",
        Warn = "",
        Info = "",
        Hint = "",
    }

    for type, icon in pairs(signs) do
        local hl = "DiagnosticSign" .. type
        vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
    end
end

M.open_float = function()
    vim.diagnostic.open_float({
        scope = "line",
        focus = false,
        source = "always",
        format = function(diag)
            return string.format(
                "%s (%s)",
                diag.message,
                diag.code or (diag.user_data and diag.user_data.lsp and diag.user_data.lsp.code)
            )
        end,
    })
end

return M
