local lsp = vim.lsp
local null_ls_sources = require("null-ls.sources")

local M = {}

M.get_active_lsp_client_names = function()
	local client_names = vim.tbl_map(function(client)
		return client.name
	end, lsp.buf_get_clients())

	return client_names
end

M.get_active_null_ls_source_names = function()
	local ft = vim.bo.ft
	local source_names = vim.tbl_map(function(source)
		return source.name
	end, null_ls_sources.get_available(ft))

	return vim.fn.uniq(source_names)
end

M.get_filtered_lsp_client_names = function()
	local lsp_client_names = M.get_active_lsp_client_names()
	local null_ls_source_names = M.get_active_null_ls_source_names()
	local filtered_lsp_client_names = {}

	lsp_client_names = vim.tbl_filter(function(client_name)
		return client_name ~= "null-ls"
	end, lsp_client_names)

	vim.list_extend(filtered_lsp_client_names, lsp_client_names)
	vim.list_extend(filtered_lsp_client_names, null_ls_source_names)

	filtered_lsp_client_names["null-ls"] = nil

	return filtered_lsp_client_names
end

M.get_formatted_lsp_client_names = function()
	local lsp_client_names = M.get_filtered_lsp_client_names()
	return table.concat(lsp_client_names, ", ")
end

return M
