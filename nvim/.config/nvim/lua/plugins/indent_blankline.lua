require("indent_blankline").setup {
    use_treesitter = true,
    show_first_indent_level = true,
    show_trailing_blankline_indent = false,
    show_current_context = true,
    buftype_exclude = { "help", "terminal", "nofile" },
    filetype_exclude = { "packer", "alpha", "TelescopePrompt", "NvimTree" },
    char = "│",
    char_highlight_list = { "Red", "Green", "Orange", "Blue", "Purple", "Aqua" }
}
