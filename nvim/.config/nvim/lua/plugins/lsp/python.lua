local M = {}

function M.setup(t)
    setmetatable(t, {__index={on_attach={}}})

    local on_attach = t[1] or t.on_attach

    require("lspconfig").pyright.setup{
        on_attach=on_attach
    }
end


return M
