local navic = require("nvim-navic")
local nvim_set_hl = vim.api.nvim_set_hl
local utils = require("utils")
local colors = require("theme").colors
local M = {}

M.update_hl = function()
    local base_val = { default = true, bg = colors.bg0 }
    local tbl_extend = utils.generate_tbl_extend_function("force", base_val)

    nvim_set_hl(0, "NavicIconsFile", tbl_extend({ fg = colors.aqua }))
    nvim_set_hl(0, "NavicIconsModule", tbl_extend({ fg = colors.yellow }))
    nvim_set_hl(0, "NavicIconsNamespace", tbl_extend({ fg = colors.purple }))
    nvim_set_hl(0, "NavicIconsPackage", tbl_extend({ fg = colors.purple }))
    nvim_set_hl(0, "NavicIconsClass", tbl_extend({ fg = colors.yellow }))
    nvim_set_hl(0, "NavicIconsMethod", tbl_extend({ fg = colors.green }))
    nvim_set_hl(0, "NavicIconsProperty", tbl_extend({ fg = colors.blue }))
    nvim_set_hl(0, "NavicIconsField", tbl_extend({ fg = colors.green }))
    nvim_set_hl(0, "NavicIconsConstructor", tbl_extend({ fg = colors.green }))
    nvim_set_hl(0, "NavicIconsEnum", tbl_extend({ fg = colors.yellow }))
    nvim_set_hl(0, "NavicIconsInterface", tbl_extend({ fg = colors.yellow }))
    nvim_set_hl(0, "NavicIconsFunction", tbl_extend({ fg = colors.green }))
    nvim_set_hl(0, "NavicIconsVariable", tbl_extend({ fg = colors.blue }))
    nvim_set_hl(0, "NavicIconsConstant", tbl_extend({ fg = colors.blue }))
    nvim_set_hl(0, "NavicIconsString", tbl_extend({ fg = colors.aqua }))
    nvim_set_hl(0, "NavicIconsNumber", tbl_extend({ fg = colors.aqua }))
    nvim_set_hl(0, "NavicIconsBoolean", tbl_extend({ fg = colors.aqua }))
    nvim_set_hl(0, "NavicIconsArray", tbl_extend({ fg = colors.aqua }))
    nvim_set_hl(0, "NavicIconsObject", tbl_extend({ fg = colors.aqua }))
    nvim_set_hl(0, "NavicIconsKey", tbl_extend({ fg = colors.red }))
    nvim_set_hl(0, "NavicIconsNull", tbl_extend({ fg = colors.aqua }))
    nvim_set_hl(0, "NavicIconsEnumMember", tbl_extend({ fg = colors.purple }))
    nvim_set_hl(0, "NavicIconsStruct", tbl_extend({ fg = colors.yellow }))
    nvim_set_hl(0, "NavicIconsEvent", tbl_extend({ fg = colors.orange }))
    nvim_set_hl(0, "NavicIconsOperator", tbl_extend({ fg = colors.orange }))
    nvim_set_hl(0, "NavicIconsTypeParameter", tbl_extend({ fg = colors.yellow }))
    nvim_set_hl(0, "NavicText", tbl_extend({ fg = colors.grey0 }))
    nvim_set_hl(0, "NavicSeparator", tbl_extend({ fg = colors.bg2 }))
end

M.setup = function()
    navic.setup({
        highlight = true,
    })
end

return M
