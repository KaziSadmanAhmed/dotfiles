local M = {}

M.hexokinase = function()
	vim.g.Hexokinase_highlighters = { "virtual" }
	vim.g.Hexokinase_ftDisabled = { "alpha", "TelescopePrompt", "NvimTree" }
end

return M
