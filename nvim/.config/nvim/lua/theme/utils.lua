local M = {}

M.update_hl = function()
    require("plugins.navic").update_hl()
end

return M
