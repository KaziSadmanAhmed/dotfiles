local wezterm = require("wezterm")

return wezterm.font_with_fallback({
    "Fisa Code",
    "Source Han Code JP",
    "SolaimanLipi",
    "Symbola",
    "JoyPixels",
})
