local action = require("wezterm").action

local M = {}

M.keys = {
    { key = "=", mods = "CTRL", action = action.IncreaseFontSize },
    { key = "-", mods = "CTRL", action = action.DecreaseFontSize },
    { key = "0", mods = "CTRL", action = action.ResetFontSize },
    { key = "c", mods = "ALT", action = action.CopyTo("ClipboardAndPrimarySelection") },
    { key = "c", mods = "LEADER", action = action.ActivateCopyMode },
    { key = "q", mods = "LEADER", action = action.QuickSelect },
    { key = "v", mods = "CTRL|SHIFT", action = action.PasteFrom("Clipboard") },
    { key = "v", mods = "CTRL|ALT", action = action.PasteFrom("PrimarySelection") },
    { key = "f", mods = "LEADER", action = action.Search("CurrentSelectionOrEmptyString") },
    {
        key = "Enter",
        mods = "CTRL|SHIFT",
        action = action.SplitHorizontal({ domain = "CurrentPaneDomain" }),
    },
    {
        key = "Enter",
        mods = "CTRL|ALT",
        action = action.SplitVertical({ domain = "CurrentPaneDomain" }),
    },
    {
        key = "w",
        mods = "CTRL|SHIFT",
        action = action.CloseCurrentPane({ confirm = true }),
    },
    {
        key = "h",
        mods = "CTRL|SHIFT",
        action = action.ActivatePaneDirection("Left"),
    },
    {
        key = "j",
        mods = "CTRL|SHIFT",
        action = action.ActivatePaneDirection("Down"),
    },
    {
        key = "k",
        mods = "CTRL|SHIFT",
        action = action.ActivatePaneDirection("Up"),
    },
    {
        key = "l",
        mods = "CTRL|SHIFT",
        action = action.ActivatePaneDirection("Right"),
    },
    {
        key = "{",
        mods = "CTRL|SHIFT",
        action = action.ActivatePaneDirection("Prev"),
    },
    {
        key = "}",
        mods = "CTRL|SHIFT",
        action = action.ActivatePaneDirection("Next"),
    },
    {
        key = "a",
        mods = "LEADER",
        action = action.PaneSelect({
            mode = "Activate",
        }),
    },
    {
        key = "s",
        mods = "LEADER",
        action = action.PaneSelect({
            mode = "SwapWithActive",
        }),
    },
    {
        key = "h",
        mods = "CTRL|SHIFT|ALT",
        action = action.AdjustPaneSize({ "Left", 1 }),
    },
    {
        key = "j",
        mods = "CTRL|SHIFT|ALT",
        action = action.AdjustPaneSize({ "Down", 1 }),
    },
    {
        key = "k",
        mods = "CTRL|SHIFT|ALT",
        action = action.AdjustPaneSize({ "Up", 1 }),
    },
    {
        key = "l",
        mods = "CTRL|SHIFT|ALT",
        action = action.AdjustPaneSize({ "Right", 1 }),
    },
    {
        key = "Enter",
        mods = "CTRL",
        action = action.SpawnTab("CurrentPaneDomain"),
    },
    {
        key = "t",
        mods = "CTRL|SHIFT",
        action = action.SpawnTab("CurrentPaneDomain"),
    },
    {
        key = "w",
        mods = "CTRL|ALT",
        action = action.CloseCurrentTab({ confirm = true }),
    },
    { key = "[", mods = "CTRL", action = action.ActivateTabRelative(-1) },
    { key = "]", mods = "CTRL", action = action.ActivateTabRelative(1) },
    { key = "`", mods = "CTRL", action = action.ActivateLastTab },
    { key = "[", mods = "CTRL|ALT", action = action.MoveTabRelative(-1) },
    { key = "]", mods = "CTRL|ALT", action = action.MoveTabRelative(1) },
}

-- CTRL+ALT+number[1-9] to activate that tab
for i = 1, 9 do
    table.insert(M.keys, {
        key = tostring(i),
        mods = "CTRL",
        action = action.ActivateTab(i - 1),
    })
end

-- CTRL+ALT+number[1-9] to move to that position
for i = 1, 8 do
    table.insert(M.keys, {
        key = tostring(i),
        mods = "CTRL|ALT",
        action = action.MoveTab(i - 1),
    })
end

M.leader = { key = "Space", mods = "CTRL|SHIFT", timeout_milliseconds = 1000 }

return M
