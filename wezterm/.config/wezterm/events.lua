local wezterm = require("wezterm")
local colors = require("themes.colors")

local ACTIVE_TAB_PREFIX = "┃"
local UNSEEN_OUTPUT_PREFIX = "ﱣ"

local function get_unseen_output(tab)
    local has_unseen_output = false
    for _, pane in ipairs(tab.panes) do
        if pane.has_unseen_output then
            has_unseen_output = true
            break
        end
    end

    return has_unseen_output
end

wezterm.on("format-tab-title", function(tab)
    local format_item = {
        { Text = " " .. tab.active_pane.title .. " " },
    }

    local has_unseen_output = get_unseen_output(tab)

    if has_unseen_output then
        table.insert(format_item, 1, { Foreground = { Color = colors.orange } })
        table.insert(format_item, 2, { Text = " " .. UNSEEN_OUTPUT_PREFIX })
        table.insert(format_item, 3, "ResetAttributes")
    end

    if tab.is_active then
        table.insert(format_item, 1, { Text = ACTIVE_TAB_PREFIX })
    end

    return format_item
end)
